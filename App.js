import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import loginPage from './src/screen/loginPage';
import MainPage from './src/screen/MainPage';
import welcomePage from './src/screen/welcomePage';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="welcomePage">
      <Stack.Screen
          name="welcomePage"
          component={welcomePage}
          options={{
            headerShown:false
          }}
        />
        <Stack.Screen
          name="loginPage"
          component={loginPage}
          options={{
            headerShown:false
          }}
        />
        <Stack.Screen
          name="MainPage"
          component={MainPage}
          options={{
            headerShown:false
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
