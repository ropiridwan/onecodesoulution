import React, {useEffect} from 'react';
import {TouchableOpacity, View, Text, StyleSheet, Image} from 'react-native';
import axios from 'axios';

const welcomePage = ({navigation}) => {
  const [user, setUser] = React.useState([]);
  const [result, setResult] = React.useState([]);

  function goLogin() {
    navigation.navigate('loginPage', {
      user,
      result,
    });
  }

  useEffect(() => {
    axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then(async response => {
        await setUser(response.data);
        await getData();
      });
  }, []);

  const getData = async () => {
    await axios
      .get('https://jsonplaceholder.typicode.com/posts')
      .then(async response => {
        await setResult(response.data);
      });
  };
  return (
    <View style={Styles.container}>
      <Image
        style={{width: '100%', height: '100%'}}
        source={{
          uri: 'https://i.pinimg.com/736x/fb/88/9a/fb889ae939a4e49001598a35f9a502b1.jpg',
        }}
      />
      <View style={Styles.containerTop}>
        <Text style={{fontSize: 16, color: 'black', fontWeight: '700'}}>
          Cinta Coding
        </Text>
        <TouchableOpacity
          style={Styles.containerButton}
          onPress={() => {
            goLogin();
          }}>
          <Text style={{fontSize: 16, color: 'white', fontWeight: '700'}}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  containerTop: {
    flexDirection: 'row',
    padding: 16,
    justifyContent: 'space-between',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  container: {flex: 1},
  containerButton: {
    backgroundColor: '#0096FF',
    paddingHorizontal: 24,
    paddingVertical: 8,
    borderRadius: 16,
  },
});

export default welcomePage;
