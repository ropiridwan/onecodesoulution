import React, {useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Alert,
} from 'react-native';

const loginPage = (props) => {
    const { navigation } = props;
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const {user, result} = props.route.params;

  const loginButton = async () => {
    const dataUser = user.filter(function (item) {
      return item.username == username && item.username == password;
    });
    // console.log('datavalid ', dataUser);

    if (dataUser.length === 0) {
      Alert.alert('Username atau Password salah !');
    } else {
      await setUsername('');
      await setPassword('');
      await Alert.alert('Berhasil Login !');
      navigation.navigate('MainPage', {
        user,
        result,
        dataUser
      });
    }
  };

  const dataDisabled = username === '' ? true : false;
  const dataDisabled2 = password === '' ? true : false;
  const dataValid = dataDisabled && dataDisabled2 ? true : false;

  return (
    <View style={Styles.container}>
      <Text style={Styles.textBold}>Login Page</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setUsername}
          style={{
            color: 'black',
            padding: 10,
            alignSelf: 'center',
          }}
          value={username}
          placeholder="username"
          placeholderTextColor={'gray'}
        />
      </View>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setPassword}
          style={{
            color: 'black',
            padding: 10,
            alignSelf: 'center',
          }}
          value={password}
          placeholder="password"
          placeholderTextColor={'gray'}
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity
        disabled={dataValid}
        onPress={() => {
          loginButton();
        }}
        style={[
          Styles.containerTouchable,
          {
            backgroundColor: '#0096FF',
            marginTop: 20,
            paddingVertical: 8,
          },
        ]}>
        <Text
          style={[
            Styles.textBold,
            {alignSelf: 'center', marginVertical: 0, color: '#fff'},
          ]}>
          Login
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  containerTouchable: {
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#0096FF',
    borderRadius: 16,
    width: 150,
    height: 40,
    marginVertical: 16,
  },
});

export default loginPage;
