import React, {useEffect} from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Alert,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';

const MainPage = (props, {navigation}) => {

  const [title, setTitle] = React.useState('');
  const [count, setCount] = React.useState(10);
  const [totalCommnet, setTotalComment] = React.useState([]);
  const [allComments, setAllComments] = React.useState([]);
  const [postID, setPostID] = React.useState([]);
  const [dataUser2, setDataUser2] = React.useState([]);
  const [userData, setUserData] = React.useState([]);
  const {user, result, dataUser} = props.route.params;

  const onChangeText = text => {
    setTitle(text);
  };

  const backButton = () => {
    setPostID([]);
    setAllComments([]);
    setDataUser2([]);
  };

  const backButtonUser = () => {
    setUserData([]);
  };

  const userInfo = async id => {
    let tamp = [];
    await axios
      .get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(async response => {
        tamp.push(response.data);
      });

    await setUserData(tamp);
  };
  const showComment = async () => {
    await axios
      .get(`https://jsonplaceholder.typicode.com/posts/${postID.id}/comments`)
      .then(async response => {
        await setAllComments(response.data);
      });
  };
  const openModal = async (id, user) => {
    setDataUser2(user);
    await axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then(async response => {
        await setPostID(response.data);
      });
    await axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
      .then(async response => {
        await setTotalComment(response.data);
      });
  };

  const dataUserFunc = (item, index) => {
    return (
      <TouchableOpacity
        onPress={() => {
          userInfo(item.id);
        }}>
        <Text
          key={index}
          style={[
            Styles.textBold,
            {marginVertical: 10, color: '#0096FF'},
          ]}>
          {item.username}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderName = (item, index) => {
    return (
      <Text key={index} style={[Styles.textBold, {width: '40%'}]}>
        {item.name}
      </Text>
    );
  };

  const loadMoreItem = (item, index) => {
    Alert.alert('Mohon Tunggu Sebentar');
    setCount(count+10)
  }
  const renderFooter = (item, index) => {
    return (
      <View style={{marginVertical: 16, alignSelf: 'center'}}>
        <ActivityIndicator size={'large'} color="#aaa" />
      </View>
    );
  };

  const renderItem = (item, index) => {
    const dataValidd = user.filter(function (_item) {
      return item.item.userId == _item.id;
    });
    return (
      <View key={index} style={Styles.containerItem}>
        {dataValidd.map((_item, _index) => renderName(_item, _index))}
        <View style={{width: '60%'}}>
          <Text numberOfLines={2} style={[Styles.textRegular, {}]}>
            {item.item.title}
          </Text>
          <View style={{flexDirection: 'row', marginVertical: 10}}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
              }}>
              <Ionicons
                style={[Styles.icon2, {marginRight: 10}]}
                name="chatbubble-outline"
              />
              <Text style={[Styles.textRegular, {color: '#0096FF'}]}>
                {totalCommnet.length == 0 ? 5 : totalCommnet.length}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginLeft: 20}}
              onPress={() => {
                openModal(item.item.id, dataValidd);
              }}>
              <Text style={[Styles.textRegular, {color: '#0096FF'}]}>
                Detail
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <>
      <View style={Styles.container}>
        <View style={Styles.containerTop}>
          <Text
            style={[
              Styles.textBold,
              {alignSelf: 'center', marginVertical: 10},
            ]}>
            Cinta Coding
          </Text>
          <Text
            style={[
              Styles.textBold,
              {
                alignSelf: 'center',
                marginVertical: 10,
                borderBottomColor: '#0096FF',
                borderBottomWidth: 1,
              },
            ]}>
            Post
          </Text>
          <View style={{flexDirection:'row'}}>
            <Text
              style={[
                Styles.textBold,
                { alignSelf:'center'},
              ]}>
              Welcome,{' '}
            </Text>
            {dataUser.map((_item, _index) => dataUserFunc(_item, _index))}
          </View>
        </View>

        {postID.length == 0 && userData.length === 0 ? (
          <>
            <View style={Styles.containerAddItemStyling}>
              <TextInput
                style={{alignSelf: 'center'}}
                placeholder="Search"
                placeholderTextColor={'black'}
                onChangeText={onChangeText}
                value={title}
              />
              <TouchableOpacity
                style={{alignSelf: 'flex-end'}}
              >
                <Ionicons style={Styles.icon} name="search-outline" />
              </TouchableOpacity>
            </View>
            <FlatList
            data={result.slice(0, count)}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              nestedScrollEnabled
              ListFooterComponent={renderFooter}
              initialNumToRender={10}
              maxToRenderPerBatch={10}
              onEndReached={loadMoreItem}
              onEndReachedThreshold={0}
            />
          </>
        ) : null}
        {postID.title !== undefined && userData.length === 0 ? (
          <>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '80%',
                alignSelf: 'center',
                marginBottom: 16,
              }}>
              <TouchableOpacity
                onPress={() => {
                  backButton();
                }}>
                <Ionicons
                  style={[Styles.icon3, {marginRight: 10}]}
                  name="arrow-back-outline"
                />
              </TouchableOpacity>
              <Text
                numberOfLines={2}
                style={[
                  Styles.textBold,
                  {
                    width: '50%',
                    marginRight: 30,
                  },
                ]}>
                {postID.title}
              </Text>
            </View>
            <View style={[Styles.containerItem, {}]}>
              {dataUser2.map((_item, _index) => renderName(_item, _index))}
              <View style={{width: '60%'}}>
                <Text numberOfLines={2} style={[Styles.textRegular, {}]}>
                  {postID.body}
                </Text>
                {allComments.length === 0 ? (
                  <View style={{flexDirection: 'row', marginVertical: 10}}>
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                      }}
                      onPress={() => {
                        showComment();
                      }}>
                      <Ionicons
                        style={[Styles.icon2, {marginRight: 10}]}
                        name="chatbubble-outline"
                      />
                      <Text style={[Styles.textRegular, {color: '#0096FF'}]}>
                        {totalCommnet.length == 0 ? 5 : totalCommnet.length}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <>
                    <Text
                      style={[
                        Styles.textRegular,
                        {fontSize: 16, marginVertical: 16},
                      ]}>
                      All Comment
                    </Text>
                    {allComments.map((__item, __index) => {
                      return (
                        <>
                          <View
                            key={__index}
                            style={{flexDirection: 'row', marginBottom: 16}}>
                            <Text
                              numberOfLines={2}
                              style={[
                                Styles.textBold,
                                {width: '40%', marginRight: 4},
                              ]}>
                              {__item.name}
                            </Text>
                            <Text
                              numberOfLines={2}
                              style={[Styles.textRegular, {width: '60%'}]}>
                              {__item.body}
                            </Text>
                          </View>
                        </>
                      );
                    })}
                  </>
                )}
              </View>
            </View>
          </>
        ) : null}
        {userData.length !== 0 ? (
          <>
            <View
              style={{
                justifyContent: 'space-between',
                width: '70%',
                alignSelf: 'center',
                marginBottom: 16,
              }}>
              <TouchableOpacity
                onPress={() => {
                  backButtonUser();
                }}>
                <Ionicons
                  style={[Styles.icon3, {marginRight: 10}]}
                  name="arrow-back-outline"
                />
              </TouchableOpacity>
              {userData.map(item_ => {
                return (
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={[
                          Styles.textRegular,
                          {
                            alignSelf: 'center',
                            marginVertical: 10,
                            fontSize: 16,
                            width:'30%'
                          },
                        ]}>
                        UserName 
                      </Text>
                      <Text
                        style={[
                          Styles.textBold,
                          {alignSelf: 'center', marginVertical: 10},
                        ]}>
                        :{'            '}{item_.username}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={[
                          Styles.textRegular,
                          {
                            alignSelf: 'center',
                            marginVertical: 10,
                            fontSize: 16,
                            width:'30%'
                          },
                        ]}>
                        Email 
                      </Text>
                      <Text
                        style={[
                          Styles.textBold,
                          {alignSelf: 'center', marginVertical: 10},
                        ]}>
                        :{'            '}{item_.email}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={[
                          Styles.textRegular,
                          {
                            alignSelf: 'center',
                            marginVertical: 10,
                            fontSize: 16,
                            width:'30%'
                          },
                        ]}>
                        Address 
                      </Text>

                      <Text
                        style={[
                          Styles.textBold,
                          {alignSelf: 'center', marginVertical: 10},
                        ]}>
                        :{'            '}{item_.address.city}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={[
                          Styles.textRegular,
                          {
                            alignSelf: 'center',
                            marginVertical: 10,
                            fontSize: 16,
                            width:'30%'
                          },
                        ]}>
                        Phone 
                      </Text>

                      <Text
                        style={[
                          Styles.textBold,
                          {alignSelf: 'center', marginVertical: 10},
                        ]}>
                        :{'            '}{item_.phone}
                      </Text>
                    </View>
                  </View>
                );
              })}
            </View>
          </>
        ) : null}
      </View>
    </>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  containerTop: {
    flexDirection: 'row',
    padding: 16,
    justifyContent: 'space-between',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
  },
  textRegular: {
    fontSize: 14,
    fontWeight: '700',
    color: 'gray'
  },
  icon: {fontSize: 25, color: 'black', alignSelf: 'center'},
  icon2: {fontSize: 16, color: '#0096FF'},
  icon3: {fontSize: 40, color: 'gray'},
  containerItem: {
    width: '80%',
    height: 60,
    marginTop: 20,
    marginBottom: 30,
    // borderWidth: 1,
    // borderColor: 'black',
    alignSelf: 'center',
    marginHorizontal: 24,
    flexDirection: 'row',
  },
  containerAddItemStyling: {
    // borderWidth: 1,
    // borderColor: 'black',
    flexDirection: 'row',
    height: 50,
    width: '80%',
    borderRadius: 24,
    // marginTop: 10,
    paddingVertical: 8,
    color: 'black',
    backgroundColor: 'gray',
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    alignSelf: 'center',
    opacity: 0.3,
  },
});

export default MainPage;
